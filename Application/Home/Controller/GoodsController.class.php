<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;
/**
 * 商品首页控制器
 * 获取首页商品数据及显示
 */
class GoodsController extends HomeController {	
	/* 头部方法
	 * 获取商城的导航栏
	 * */
	public function head(){
		//TODO:获取导航栏数据并复制到模板
		$this->assign('navbar',$navbar);
	}
	/* 尾部方法
	 * 获取商城的底部
	 * */
	public function footer(){
		//TODO:获取尾部并复制到模板
		$this->assign('foot',$foot);
	}
	/*
	 * 商城首页，显示商城页面及商品展示柜
	 * */
	public function index(){
		//TODO:获取商品信息并展示到页面
		$this->head();
		$this->footer();
		$this->display();
	}
	
}